package consensus

import (
	"github.com/tendermint/tendermint/config"

	"ether-rbbc/network"
	stdtracer "ether-rbbc/tracer"
)

type consensusNullTracer struct {
	stdtracer.Tracer
}

func (trc consensusNullTracer) assertPersistedInitStateDb(tmtCfg *config.Config, ntw network.Network) {
	return
}
