module ether-rbbc

go 1.12

replace gopkg.in/urfave/cli.v1 => github.com/urfave/cli v1.21.0

replace github.com/prometheus/client_golang v1.1.0 => github.com/prometheus/client_golang v0.9.2

replace github.com/prometheus/prometheus => github.com/prometheus/prometheus v2.1.0+incompatible

replace github.com/ethereum/go-ethereum => /home/ubuntu/go/src/ether-rbbc/vendor/github.com/ethereum/go-ethereum  

replace rbbc => ../rbbc

require (
	github.com/allegro/bigcache v1.2.1 // indirect
	github.com/apilayer/freegeoip v3.5.0+incompatible // indirect
	github.com/aristanetworks/goarista v0.0.0-20191001182449-186a6201b8ef // indirect
	github.com/beorn7/perks v1.0.0 // indirect
	github.com/btcsuite/btcd v0.0.0-20190926002857-ba530c4abb35 // indirect
	github.com/deckarep/golang-set v1.7.1 // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/elastic/gosigar v0.10.5 // indirect
	github.com/ethereum/go-ethereum v1.8.27
	github.com/fjl/memsize v0.0.0-20190710130421-bcb5799ab5e5 // indirect
	github.com/gballet/go-libpcsclite v0.0.0-20190607065134-2772fd86a8ff // indirect
	github.com/go-kit/kit v0.9.0
	github.com/gogo/protobuf v1.3.0 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/graph-gophers/graphql-go v0.0.0-20190917030536-38a077bc812d // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/huin/goupnp v1.0.0 // indirect
	github.com/influxdata/influxdb v1.7.8 // indirect
	github.com/jackpal/go-nat-pmp v1.0.1 // indirect
	github.com/karalabe/hid v1.0.0 // indirect
	github.com/karalabe/usb v0.0.0-20190919080040-51dc0efba356 // indirect
	github.com/lightstreams-network/lightchain v1.3.1
	github.com/mattn/go-colorable v0.1.4
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/olekukonko/tablewriter v0.0.1 // indirect
	github.com/oschwald/maxminddb-golang v1.5.0 // indirect
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/prometheus/client_golang v1.1.0
	github.com/prometheus/client_model v0.0.0-20190129233127-fd36f4220a90 // indirect
	github.com/prometheus/common v0.4.1 // indirect
	github.com/prometheus/procfs v0.0.2 // indirect
	github.com/prometheus/prometheus v0.0.0-00010101000000-000000000000 // indirect
	github.com/prometheus/tsdb v0.10.0 // indirect
	github.com/rjeczalik/notify v0.9.2 // indirect
	github.com/rs/cors v1.7.0 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
	github.com/status-im/keycard-go v0.0.0-20190424133014-d95853db0f48 // indirect
	github.com/steakknife/bloomfilter v0.0.0-20180922174646-6819c0d2a570 // indirect
	github.com/steakknife/hamming v0.0.0-20180906055917-c99c65617cd3 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/tendermint/go-amino v0.14.1
	github.com/tendermint/tendermint v0.31.5
	github.com/tyler-smith/go-bip39 v1.0.2 // indirect
	github.com/wsddn/go-ecdh v0.0.0-20161211032359-48726bab9208 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/crypto v0.0.0-20191002192127-34f69633bfdc
	gopkg.in/olebedev/go-duktape.v3 v3.0.0-20190709231704-1e4459ed25ff // indirect
	gopkg.in/urfave/cli.v1 v1.0.0-00010101000000-000000000000
	rbbc v0.0.0-00010101000000-000000000000
)
