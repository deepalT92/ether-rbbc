# CollaChain EVM

[![License: GPLv3](https://img.shields.io/badge/License-GPLv3-green.svg)](
https://hades.it.usyd.edu.au/collachain/ether-rbbc/blob/master/LICENSE)

The CollaChain EVM is a variant of the go EVM (geth) optimized to commit many transactions per second and discard unnecessary validations.

## Authors
Deepal Tennakoon (dten6395@uni.sydney.edu.au), Yiding Hua, Vincent Gramoli
## Requirements

[![Go version](https://img.shields.io/badge/go-1.12.0-blue.svg)]() - [![Go version](https://img.shields.io/badge/go-1.13.1-blue.svg)]()
[![Ubuntu versions](https://img.shields.io/badge/ubuntu-20.04LTS-orange)](), [![Ubuntu versions](https://img.shields.io/badge/ubuntu-18.04LTS-orange)]()

16GB of RAM per Collachain node is required if executing additional web3js files

Install Go:
```bash
mkdir -p $HOME/go/src/
wget https://dl.google.com/go/go1.13.1.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.13.1.linux-amd64.tar.gz
```

Configure Go:
```bash
echo "export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
export GOROOT=/usr/local/go
export GOPATH=$HOME/go" >> .profile
source .profile
mkdir ~/go/bin
```

Install dependencies, including make:

```bash 
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | bash
sudo apt-get update --fix-missing && sudo apt-get -y install build-essential
```

Install gcc on the state node:
```bash
sudo apt-get install gcc ; make install
```

## Install

Download the source of the optimized EVM:
```bash
cd $HOME/go/src
git clone https://hades.it.usyd.edu.au/collachain/ether-rbbc.git
cd $HOME/go/src/ether-rbbc
```

Copy the [consensus](rbbc) source across for the optimized EVM to build
(now you are copying rbbc to ether-rbbc vendor folder):
```bash
cp $HOME/go/src/rbbc ~/go/src/ether-rbbc/vendor 
```

Install the optimized EVM:
```bash
. ~/.profile
cd $HOME/go/src/ether-rbbc
make install
```

## Configure
Note that you may not need to create `rbcore` (as below) if you run the consensus 
`rbbc` on the same machine.
```
cd $HOME
mkdir rbcore
mv $HOME/go/src/ether-rbbc/observer.yaml ~/rbcore
mv $HOME/go/src/ether-rbcc/genesis.json ~/
```
Change the [observer.yaml](observer.yaml) with the local IP (as seen on the
network, not 127.0.0.1), keep the port number 9000 and set the serverAddr
field to one of the consensus node IP address with port 8000:
```
ip: 3.133.97.233
port: 9000
serverAddr: 18.220.62.141:8000
```

## Run

Configure the parameters --threshold=1 --timeout=2000 in
[observe.sh](observe.sh).

Start all state nodes using:
```bash
cp $HOME/go/src/ether-rbbc/observe.sh ~/
bash observe.sh
```

### Initialization

This command initializes Lightchain configurations in the folder
.lightchain_standalone.
The target folder can be customized by changing values of the parameter
--datadir.

```bash
rbbc init --datadir="${HOME}/.lightchain_standalone" --standalone
```

### Run EVM

This command runs the EVM state machine with the following customizable configurations
* Threshold: 300
* Timeout: 500
* Lightchain Directory: ${HOME}/.lightchain_standalone
* DBFT Directory: $HOME/rbcore
* RPC port: 8545

```bash
rbbc run --threshold=300 --timeout=500 --datadir="${HOME}/.lightchain_standalone" --rpc --rpcaddr=0.0.0.0 --rpcport=8545 --rpcapi eth,net,web3,personal,admin --dbftDir=$HOME/rbcore
```

Parameters:
* The Threshold is the maximum number of transactions received before making a proposal.
* The Timout is the maximum duration between two proposals.
* The RPC port is the port on which the Ethereum services are hosted.

## Modifications

A few changes have to be made to adapt Lightchain to meet our needs.

### DBFT

The observer server is implemented in folder [dbft](./dbft).

The [node.go](./dbft/node.go) implements an Observer server that listens for commit message from Rbcore and commits Ethereum transactions.
The most important line is the [node.db.ExecuteTx(tx)](https://hades.it.usyd.edu.au/yhua/ether-rbbc/blob/master/dbft/node.go#L90) which executes transactions using EVM.
The whole execution lifecycle can be tracked from this line.

The [txmanager](./dbft/txmanager.go) is the data structure used to manage transactions and make proposals.
It receives transaction and batches them.

### Node

The [node.go](./node/node.go) has to be modified to use Rbcore for consensus instead of Tendermint.

The [config.go](./node/config.go) also has to be modified to add new parameters.