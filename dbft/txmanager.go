package dbft

import (
	"encoding/json"
	"github.com/ethereum/go-ethereum/core/types"
)

type txManager struct {
	txpool    []*types.Transaction
	threshold int
}

func (txm *txManager) add(tx *types.Transaction) {
	txm.txpool = append(txm.txpool, tx)
}

func (txm *txManager) check() bool {
	return len(txm.txpool) >= txm.threshold
}

func (txm *txManager) size() int {
	return len(txm.txpool)
}

func (txm *txManager) serialize() []byte {
	data, _ := json.Marshal(txm.txpool)
	txm.txpool = make([]*types.Transaction, 0)
	return data
}

func (txm *txManager) deserialize(data []byte) []*types.Transaction {
	var txs []*types.Transaction
	err := json.Unmarshal(data, &txs)
	if err != nil {

	}
	return txs
}
