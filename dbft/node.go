package dbft

import (
	"ether-rbbc/database"
	"ether-rbbc/log"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	tmtAbciTypes "github.com/tendermint/tendermint/abci/types"
	tmtLog "github.com/tendermint/tendermint/libs/log"
	"rbbc/configurations"
	"rbbc/grpc"
	"time"
)

const (
	timerBufferSize  = 128
	commitBufferSize = 128
)
//var super = make([]*types.Transaction, 100000)
type Node struct {
	id               int
	started          bool
	addr             string
	proposeChan      chan *types.Transaction
	commitChan       chan [][]byte
	observerServer   *grpc.ObserverServer
	consensusClient  *grpc.ConsensusClient
	db               *database.Database
	txm              *txManager
	logger           tmtLog.Logger
	threshold        int
	timeout          int
	timeoutChan      chan bool
	restartTimerChan chan bool
}

func NewNode(
	id int,
	observerConfig *configurations.ObserverConfig,
	proposeChan chan *types.Transaction,
	threshold int,
	timeout int) *Node {
	commitChan := make(chan [][]byte, commitBufferSize)
	return &Node{
		id:              id,
		addr:            fmt.Sprintf("%s:%d", observerConfig.Ip, observerConfig.Port),
		commitChan:      commitChan,
		observerServer:  grpc.NewObserverServer(observerConfig.GenerateServerConfig(), commitChan),
		consensusClient: grpc.NewConsensusClient(observerConfig.GenerateClientConfig()),
		txm: &txManager{
			threshold: threshold,
		},
		logger:           log.NewLogger().With("engine", "observer"),
		proposeChan:      proposeChan,
		threshold:        threshold,
		timeout:          timeout,
		timeoutChan:      make(chan bool, timerBufferSize),
		restartTimerChan: make(chan bool, timerBufferSize),
	}
}

func (node *Node) Start(db *database.Database) {
	node.db = db

	if node.started {
		return
	}

	node.logger.Info(fmt.Sprintf("Starting DBFT Server. Block Threshold: %d, Block Timeout: %d", node.threshold, node.timeout))

	node.observerServer.Start()

	node.consensusClient.Subscribe(node.addr)

	// Listen to commitChan for decided super blocks
	// and commit super block upon reception.
	go func() {

		for block := range node.commitChan {
			//tot := 0
			total := 0

			fmt.Printf("COMMIT: %d\n", time.Now().UnixNano()/1000000)

			for _, proposal := range block {
				//super = append(super,proposal...)
				txs := node.txm.deserialize(proposal)
				//super = append(super, txs...)
				node.db.UpdateBlockState(&tmtAbciTypes.Header{Time: time.Now(), NumTxs: int64(len(txs))})

				for _, tx := range txs {
					node.db.ExecuteTx(tx)
				}

				node.db.Persist(common.Address{})
				total += len(txs)
			}
			//txs := node.txm.deserialize(super)
			//node.db.UpdateBlockState(&tmtAbciTypes.Header{Time: time.Now(), NumTxs: int64(total)})
			//fmt.Println("This is the tot value:", tot)
			//for i:=0; i < tot; i++{
				//node.db.ExecuteTx(super[i])
			//}
			//for _, tx := range super {
				//node.db.ExecuteTx(tx)
			//}
			fmt.Printf("Number of Transactions: %d\n", total)
			
			//fmt.Println("Common Address", common.Address{})

			//node.db.Persist(common.Address{})
			//super = make([]*types.Transaction, 0)
		}
	}()

	// Timer go routine tracks timeout status.
	// A timer waits for certain timeout and notifies timeoutChan for timeout event.
	// Timer is reset when timer received message from restartTimerChan/
	go func() {
		for {
			select {
			case <-time.After(time.Duration(node.timeout) * time.Millisecond):
				node.timeoutChan <- true
			case <-node.restartTimerChan:
			}
		}
	}()

	// Proposal handling go rountine listens to proposeChan and timeoutChan.
	// A proposal is made in either of the following cases:
	// - Tx pool has reached its threshold;
	// - Timer has expired.
	go func() {
		for {
			select {
			case tx := <-node.proposeChan:
				node.txm.add(tx)
				if node.txm.check() {
					// Reset timer after a proposal is made
					go func() {
						node.restartTimerChan <- true
					}()

					// Make proposal
					node.consensusClient.Propose(node.txm.serialize())
				}
			case <-node.timeoutChan:
				// Make proposal is timer has expired
				// and tx pool is not empty.
				if node.txm.size() > 0 {
					node.consensusClient.Propose(node.txm.serialize())
				}
			}
		}
	}()
}
