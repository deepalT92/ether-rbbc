#!/bin/sh
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
export GOROOT=/usr/local/go
export GOPATH=$HOME/go

rm -rf ${HOME}/.lightchain_standalone

# Initialize rbbc

rbbc init --datadir="${HOME}/.lightchain_standalone" --standalone
rm -rf /home/ubuntu/.lightchain_standalone/database/chaindata
rm -rf /home/ubuntu/.lightchain_standalone/database/nodes
cp genesis.json /home/ubuntu/.lightchain_standalone/database/
# Run EVM
rm -f log

rbbc run --threshold=50 --timeout=2000 --datadir="${HOME}/.lightchain_standalone" --rpc --rpcaddr=0.0.0.0 --rpcport=8545 --rpcapi eth,net,web3,personal,admin --ws --wsaddr=0.0.0.0 --wsport=8546 --wsorigins="*" --wsapi eth,net,web3,personal,admin --dbftDir=$HOME/rbcore >> log
