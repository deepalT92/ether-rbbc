#!/bin/sh
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
export GOROOT=/usr/local/go
export GOPATH=$HOME/go

# Run RbCore
rm -f log
rbcore run -d $HOME/rbcore
